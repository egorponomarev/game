package com.pgisaa.dm.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.pgisaa.dm.Main;

import sun.misc.ObjectInputFilter;

public class DesktopLauncher {
	public static void main (String[] arg) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		config.title = "Gothic II: Night of the Raven Mini ";
		config.width = 512;
		config.height=512;
		new LwjglApplication(new Main(), config);
	}
}

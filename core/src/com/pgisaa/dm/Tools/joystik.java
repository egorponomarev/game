package com.pgisaa.dm.Tools;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

/**
 * @author Ponomarev G.I.
 * Класс, содержащий в себе отрисовку эллемента управления
 */
public class joystik {
    Texture CircleImg , StickImg;
    /*
    * CircleImg - текстура окружности, джостика
    * StickImg - текстура "стика"
    */
    Circle CircleBounds, StickBounds; //Границы достика и стика
    private int pointer = -1; //Номер пальца если он убран
    float Rcircle, Rstick;
    Point2D direction;

    public joystik(Texture cimg, Texture simg, Point2D point, float Size){

        CircleImg = cimg;
        StickImg = simg;
        Rcircle = Size/2;
        Rstick = Rcircle;
        CircleBounds = new Circle(point, Rcircle);
        StickBounds = new Circle(point, Rstick);
        direction = new Point2D(0,0);//изначальное напрвление


    }

    public void draw(SpriteBatch batch){//Отрисовка джостика и стика
        batch.draw(CircleImg,CircleBounds.pos.getX()-Rcircle,CircleBounds.pos.getY()-Rcircle,Rcircle*2,Rcircle*2);
        batch.draw(StickImg,StickBounds.pos.getX()-Rstick,StickBounds.pos.getY()-Rstick, Rstick*2,Rstick*2);
    }
    public void update(float x, float y, boolean isDownTouch, int pointer){
        Point2D touch = new Point2D(x,y);// Точка касания на джостике
        if(CircleBounds.isContains(touch)&& isDownTouch && this.pointer==-1)this.pointer = pointer;

        if(CircleBounds.Overlaps(StickBounds) && isDownTouch && pointer==this.pointer)atControl(new Point2D(x,y));

        if(!isDownTouch && pointer==this.pointer)returnStick();
        if(isDownTouch&&pointer==this.pointer&&!CircleBounds.isContains(touch))returnStick();

    }
    /*
    Метод используется при управлении джостика
    Двигает стик и задает напрвление для игрока
     */
    public void atControl(Point2D point){

        StickBounds.pos.setPoint(point);
        float dx = CircleBounds.pos.getX()-StickBounds.pos.getX();
        float dy = CircleBounds.pos.getY()-StickBounds.pos.getY();
        float dist = (float)Math.sqrt(dx*dx + dy*dy);
        direction.setPoint(-(dx/dist),-(dy/dist));

    }
    public void returnStick(){
        StickBounds.pos.setPoint(StickBounds.pos);
        direction.setPoint(0,0);
        pointer=-1;
    }
    public Point2D getDir(){
        return direction;
    }
}

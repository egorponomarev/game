package com.pgisaa.dm;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.pgisaa.dm.Screens.GameSc;

public class Main extends Game {
	public static SpriteBatch batch;
	 public static Texture img;
	 public static int WIDTH, HEIGHT;
	 public static Texture joystickModel, stickModel, person,fon;

	@Override
	public void create () {
		batch = new SpriteBatch();
		WIDTH = Gdx.graphics.getWidth();// ШИрина равна ширине экрана
		HEIGHT = Gdx.graphics.getHeight();// Высота равна высоте экрана
		joystickModel = new Texture("Без имени-3.png");
		stickModel = new Texture("Без имени-2.png");
		person = new Texture("personazh.png");
		fon = new Texture("Sprite-0004.png");
		setScreen(new GameSc(this));
	}


	@Override
	public void dispose () {
		batch.dispose();
		img.dispose();
	}
}

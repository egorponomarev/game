package com.pgisaa.dm.Screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.pgisaa.dm.Actors.Mobs;
import com.pgisaa.dm.Actors.Player;
import com.pgisaa.dm.Main;
import com.pgisaa.dm.Tools.Point2D;
import com.pgisaa.dm.Tools.joystik;
import com.badlogic.gdx.graphics.Texture;

import java.util.ArrayList;


/**
 * @author Ponomarev G.I.
 * Класс реализует загрузку рабочего экрана игры
 */

public class GameSc implements Screen {
    public static ArrayList<Mobs> mobsArray;
    joystik joy;
    Player player;

    Main main;
    
    private Texture map;

    public GameSc(Main main ) {
        this.main = main;

    }
    @Override
    public void show() {
        Gdx.input.setInputProcessor(new InputProcessor() {
            @Override
            public boolean keyDown(int keycode) {
                return false;
            }

            @Override
            public boolean keyUp(int keycode) {
                return false;
            }

            @Override
            public boolean keyTyped(char character) {
                return false;
            }

            @Override
            public boolean touchDown(int screenX, int screenY, int pointer, int button) {
                screenY = Main.HEIGHT - screenY;
                multitouch(screenX,screenY,true,pointer);
                return false;

            }

            @Override
            public boolean touchUp(int screenX, int screenY, int pointer, int button) {
                screenY = Main.HEIGHT - screenY;
                multitouch((int)screenX,(int)screenY,false,pointer);
                return false;
            }

            @Override
            public boolean touchDragged(int screenX, int screenY, int pointer) {
                screenY = Main.HEIGHT - screenY;
                multitouch((int)screenX,(int)screenY,true,pointer);
                return false;
            }

            @Override
            public boolean mouseMoved(int screenX, int screenY) {
                return false;
            }

            @Override
            public boolean scrolled(int amount) {
                return false;
            }
        });

        loadElements();
    }

    @Override
    public void render(float delta) {

        Main.batch.begin();
        GameUpdate();
        GameRender(Main.batch);
        Main.batch.end();

    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
        map.dispose();

    }
    public void GameUpdate(){
        player.serDirection(joy.getDir());
        player.update();
        for(int i = 0; i < mobsArray.size();i++){
            mobsArray.get(i).update();

        }
        for(int j = 0; j < mobsArray.size();j++){
            if(mobsArray.get(j).getHealth() < 1){

            }
        }
        collision();




    }
    public void GameRender(SpriteBatch batch){
        batch.draw(map, -128f, -256f, 2560,1536);
        player.draw(batch);
        joy.draw(batch);
        for(int i = 0; i < mobsArray.size();i++){
            mobsArray.get(i).draw(batch);

        }



    }
    public void loadElements(){
        map = new Texture("map.png");
        joy = new joystik(Main.joystickModel,Main.stickModel,new Point2D(Main.WIDTH-((Main.HEIGHT/3)/2+(Main.HEIGHT/3)/4),(Main.HEIGHT/3)/2+(Main.HEIGHT/3)/4),Main.HEIGHT/3);
        player = new Player(Main.person,new Point2D(Main.WIDTH/2,Main.HEIGHT/2),5,Main.HEIGHT/20,10);
        mobsArray = new ArrayList<>();
        mobsArray.add(new Mobs(Main.stickModel,new Point2D(Main.WIDTH/2,Main.HEIGHT/4),5,15,5));





    }
    public void multitouch(float x, float y, boolean isDownTouch, int pointer){
        for(int i = 0; i < 5; i++ ){
            joy.update(x,y,isDownTouch,pointer);

        }
    }
    public void collision(){
        for(int i = 0; i < mobsArray.size();i++){
        mobsArray.get(i).hit();
        player.hit();
        }
    }
}

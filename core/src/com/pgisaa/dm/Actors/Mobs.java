package com.pgisaa.dm.Actors;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.pgisaa.dm.Main;
import com.pgisaa.dm.Tools.Point2D;



public class Mobs extends Actor {

    private float Health;



    public Mobs(Texture img, Point2D position,float Speed, float R, float  health) {
        super(img, position,  Speed, R);
        this.Health = health;


        direction.setX((float)Math.sin(Math.toRadians(Math.random()*360)));
        direction.setY((float)Math.cos(Math.toRadians(Math.random()*360)));


    }


    @Override
    public void draw(SpriteBatch batch) {
        batch.draw(img,position.getX()-R,position.getY()-R,R*R,R*R);
    }

    @Override
    public void update() {
        if (position.getX()+R > Main.WIDTH)direction.setX(-direction.getX());/*Вытеснение персонажа, чтобы он не залетат за экран
        Правая стенка*/
        if(position.getX()-R < 0)direction.setY(-direction.getX());//Левая стенка экрана
        if(position.getY()+R > Main.HEIGHT)direction.setY(-direction.getY());//Верх экрана
        if(position.getY()-R < 0)direction.setY(-direction.getY());// Нижняя часть экрана
        position.add(direction.getX()*Speed,direction.getY()*Speed);
    }
    public void hit(){
        Health--;
    }

    public float getHealth(){
        return Health;
    }

}


package com.pgisaa.dm.Actors;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.pgisaa.dm.Main;
import com.pgisaa.dm.Tools.Point2D;

public class Player extends Actor{

    private int Score;
    private float health;

    public Player(Texture img, Point2D position, float Speed, float R, float health) {
        super(img, position, Speed, R);
        this.health = health;
    }

    @Override
    public void draw(SpriteBatch batch) {
        batch.draw(img,position.getX()-R,position.getY()-R);//Чтобы картинка совпадала с координатами персонажа
    }

    @Override
    public void update() {
        if (position.getX()+R > Main.WIDTH)position.setX(Main.WIDTH-R);/*Вытеснение персонажа, чтобы он не залетат за экран
        Правая стенка*/
        if(position.getX()-R < 0)position.setX(R);//Левая стенка экрана
        if(position.getY()+R > Main.HEIGHT)position.setY(Main.HEIGHT-R);//Верх экрана
        if(position.getY()-R < 0)position.setY(R);// Нижняя часть экрана

        position.add(direction.getX()*Speed,direction.getY()*Speed);
    }

    public void hit(){
        health--;
    }
    public float getHealth(){
        return health;
    }
}

package com.pgisaa.dm.Actors;

import com.badlogic.gdx.graphics.Texture;
import com.pgisaa.dm.GraphicsObj.GraphicsObj;
import com.pgisaa.dm.Tools.Circle;
import com.pgisaa.dm.Tools.Point2D;

/**
 * @author Ponomarev G.I.
 * Класс, хранящий в себе всех персонажей(действующих лиц их характеристики и свойства)
 */

public abstract class Actor extends GraphicsObj {

    public Point2D position;    //Позиция
    public float Speed, R;      //Скорость и радиус
    public Circle bounds;       //Границы
    public Point2D direction;   //Направление движения

    public Actor(Texture img, Point2D position, float Speed, float R) {

        super(img);
        this.position = new Point2D(position);
        this.Speed = Speed;
        this.R = R;
        bounds = new Circle(position, R);
        direction = new Point2D(0, 0);

    }


    public void serDirection(Point2D dir){
        direction = dir;
    }

}
